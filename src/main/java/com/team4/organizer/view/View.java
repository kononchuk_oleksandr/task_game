/**
 * Created by Oleksandr Kononchuk
 */

package com.team4.organizer.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class View {
  private BufferedReader reader = new BufferedReader(
      new InputStreamReader(System.in));
  private Map<String, String> menu;
  private Map<String, String> menuContacts;
  private Map<String, String> menuNotes;
  private Map<String, Printable> menuButtons;
  private Map<String, Printable> menuContactsButtons;
  private Map<String, Printable> menuNotesButtons;
  private String key;

  public View() {
    menu = new LinkedHashMap<String, String>();
    menu.put("1", "1 - Contacts");
    menu.put("2", "2 - Notes");
    menu.put("E", "E - Exit");

    menuContacts = new LinkedHashMap<String, String>();
    menuContacts.put("1", "1 - Add new contact");
    menuContacts.put("2", "2 - View all contacts");
    menuContacts.put("3", "3 - View sorted contacts by name");
    menuContacts.put("4", "4 - View sorted contacts by sure name");
    menuContacts.put("5", "5 - Return to organizer");
    menuContacts.put("E", "E - Exit");

    menuNotes = new LinkedHashMap<String, String>();
    menuNotes.put("1", "1 - Add new note");
    menuNotes.put("2", "2 - View all notes");
    menuNotes.put("3", "3 - Return to organizer");
    menuNotes.put("E", "E - Exit");

    menuButtons = new LinkedHashMap<>();
    menuButtons.put("1", this::menuButton1);
    menuButtons.put("2", this::menuButton2);

    menuContactsButtons = new LinkedHashMap<>();
    menuContactsButtons.put("1", this::contactsButton1);
    menuContactsButtons.put("2", this::contactsButton2);
    menuContactsButtons.put("3", this::contactsButton3);
    menuContactsButtons.put("4", this::contactsButton4);

    menuNotesButtons = new LinkedHashMap<>();
    menuNotesButtons.put("1", this::notesButton1);
    menuNotesButtons.put("2", this::notesButton2);
  }

  private void menu() {
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  private void menuButton1() throws IOException {
    for (String str : menuContacts.values()) {
      System.out.println(str);
    }

    while (true) {
      key = reader.readLine();
      if (key.equals("5")) {
        break;
      } else {
        menuContactsButtons.get(key).print();
      }
    }
  }

  private void menuButton2() throws IOException {
    for (String str : menuNotes.values()) {
      System.out.println(str);
    }

    while (true) {
      key = reader.readLine();
      if (key.equals("3")) {
        break;
      } else {
        menuNotesButtons.get(key).print();
      }
    }
  }

  private void contactsButton1() {}
  private void contactsButton2() {}
  private void contactsButton3() {}
  private void contactsButton4() {}

  private void notesButton1() {}
  private void notesButton2() {}

  public void start() {
    System.out.println("Welcome to console organizer!\n" +
        "Please push the button:");
    do {
      try {
        menu();
        key = reader.readLine();
        menuButtons.get(key).print();
      } catch (IOException e) {
        System.out.println(e.getMessage());
      } catch (NullPointerException e) {
        System.out.println("Good bye!");
      }
    } while (!key.equalsIgnoreCase("e"));
  }
}
