/**
 * Created by Oleksandr Kononchuk.
 */

package com.team4.organizer.view;

import java.io.IOException;

@FunctionalInterface
public interface Printable {

  void print() throws IOException;

}
