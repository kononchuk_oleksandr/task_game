package com.team4.organizer.model;

public class Contacts {
  private String name;
  private String sureName;
  private String number;
  private String secondNumber;
  private String info;

  public Contacts(String name, String sureName, String number, String secondNumber, String info) {
    this.name = name;
    this.sureName = sureName;
    this.number = number;
    this.secondNumber = secondNumber;
    this.info = info;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSureName() {
    return sureName;
  }

  public void setSureName(String sureName) {
    this.sureName = sureName;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public String getSecondNumber() {
    return secondNumber;
  }

  public void setSecondNumber(String secondNumber) {
    this.secondNumber = secondNumber;
  }

  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info;
  }

  @Override
  public String toString() {
    return "Contacts{" +
        "name='" + name + '\'' +
        ", sureName='" + sureName + '\'' +
        ", number='" + number + '\'' +
        ", secondNumber='" + secondNumber + '\'' +
        ", info='" + info + '\'' +
        '}';
  }
}
