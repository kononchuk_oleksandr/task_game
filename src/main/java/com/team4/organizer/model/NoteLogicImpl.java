package com.team4.organizer.model;

import java.util.List;
import java.util.Scanner;

public class NoteLogicImpl implements NoteLogic {

    public List<Note> createNote(List<Note> list, String noteName,
                                 String noteSubject, String noteText) {
        int noteId = list.size() + 1;
        Note newNote = new Note(noteName, noteId, noteSubject, noteText);
        list.add(newNote);
        return list;
    }

    public List<Note> getSortedByName(List<Note> list) {
        list.sort(Note.SORT_BY_NAME);
        return list;
    }

    public List<Note> getSortedById(List<Note> list) {
        list.sort(Note.SORT_BY_ID);
        return list;
    }

    public List<Note> editNote(List<Note> list, int index, int choice, String addText) {
        String noteText = list.get(index).getNoteText();
        if(choice == 1){
            String result = noteText.concat(" " + addText);
            list.get(index).setNoteText(result);
        }
        else if (choice == 2){
            list.get(index).setNoteText(addText);
        }
        return list;
    }

    public List<Note> deleteNote(List<Note> list,int noteNum) {
        noteNum = noteNum - 1;
        list.remove(noteNum);
        return list;
    }
}
