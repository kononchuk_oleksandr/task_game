package com.team4.organizer.model;

import java.util.List;

public interface BusinessLogic {

  List<Contacts> createContacts();
  List<Contacts> getSortedListByName(List<Contacts> list);
  List<Contacts> getSortedListBySureName(List<Contacts> list);

}
