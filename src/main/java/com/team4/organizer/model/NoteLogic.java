package com.team4.organizer.model;

import java.util.List;

public interface NoteLogic {

    List<Note> createNote(List<Note> list, String noteName,
                          String noteSubject, String noteText);
    List<Note> getSortedByName(List<Note> list);
    List<Note> getSortedById(List<Note> list);
    List<Note> editNote(List<Note> list, int index, int choice, String addText);
    List<Note> deleteNote(List<Note> list,int noteNum);
 }
