package com.team4.organizer.model;

import java.util.Comparator;

public class Note {
    private String noteName;
    private int noteID;
    private String noteSubject;
    private String noteText;

    public Note(String noteName, int noteID, String noteSubject, String noteText) {
        this.noteName = noteName;
        this.noteID = noteID;
        this.noteSubject = noteSubject;
        this.noteText = noteText;
    }

    public String getNoteName() {
        return noteName;
    }

    public void setNoteName(String noteName) {
        this.noteName = noteName;
    }

    public int getNoteID() {
        return noteID;
    }

    public void setNoteID(int noteID) {
        this.noteID = noteID;
    }

    public String getNoteSubject() {
        return noteSubject;
    }

    public void setNoteSubject(String noteSubject) {
        this.noteSubject = noteSubject;
    }

    public String getNoteText() {
        return noteText;
    }

    public void setNoteText(String noteText) {
        this.noteText = noteText;
    }

    @Override
    public String toString() {
        return "Note{" +
                "noteName='" + noteName + '\'' +
                ", noteID='" + noteID + '\'' +
                ", noteSubject='" + noteSubject + '\'' +
                ", noteText='" + noteText + '\'' +
                '}';
    }
    public static final Comparator<Note> SORT_BY_ID = new Comparator<Note>() {
        @Override
        public int compare(Note lhs, Note rhs) {
            return lhs.getNoteID() - rhs.getNoteID();
        }
    };
    public static final Comparator<Note> SORT_BY_NAME = new Comparator<Note>() {
        @Override
        public int compare(Note obj1, Note obj2) {
            return obj1.getNoteName().compareTo(obj2.getNoteName());
        }
    };
}
