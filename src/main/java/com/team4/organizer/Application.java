package com.team4.organizer;

import com.team4.organizer.view.View;

public class Application {
  public static void main(String[] args) {
    new View().start();
  }

}
